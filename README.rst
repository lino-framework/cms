========================
The ``lino-cms`` package
========================

**Lino CMS** is a Content Management System à la Lino.

- Public demo : https://cms1.mylino.net

- Documentation: https://lino-framework.gitlab.io/cms/

- Source code: https://gitlab.com/lino-framework/cms

- This project is part of the Lino framework, which is documented
  at https://www.lino-framework.org

- Changelog: https://lino-framework.gitlab.io/cms/changes.html

- For introductions, commercial information and hosting solutions
  see https://www.saffre-rumma.net

- This is a sustainably free open-source project. Your contributions are
  welcome.  See https://community.lino-framework.org for details.
