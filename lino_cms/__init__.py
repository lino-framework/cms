# -*- coding: UTF-8 -*-
# Copyright 2016-2025 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

"""This is the Lino CMS package.

.. autosummary::
   :toctree:

   lib

"""

__version__ = '24.5.0'

srcref_url = 'https://gitlab.com/lino-framework/cms/blob/master/%s'
doc_trees = []
