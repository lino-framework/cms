# -*- coding: UTF-8 -*-
# Copyright 2013-2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Extends :mod:`lino_xl.lib.cal` for cms.

"""

from lino_xl.lib.cal import Plugin


class Plugin(Plugin):

    extends_models = ['Event', 'Room']
