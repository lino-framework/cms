# -*- coding: UTF-8 -*-
# Copyright 2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.modlib.uploads.models import *

class UploadDetail(UploadDetail):
    main = """
    left preview
    comments.CommentsByRFC memo.MentionsByTarget
    """
