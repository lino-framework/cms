# -*- coding: UTF-8 -*-
# Copyright 2012-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
# fmt: off

from lorem import get_paragraph
from django.utils import translation
from django.conf import settings
from lino.api import rt, dd, _
from lino.utils import Cycler

# Page = rt.models.publisher.Page
# Node = rt.models.publisher.Page
Page = "page"
Node = "node"

PublishingStates = rt.models.publisher.PublishingStates
SpecialPages = rt.models.publisher.SpecialPages

welcome = _("""Welcome to our great website. We are proud to present
the best content about foo, bar and baz.

Our [Services](/p/19) page is a hierarchy of pages.  Try to find the [Machine
drying](/p/23) page, which still is not the last generation.

""")

# BODIES = Cycler([lorem, short_lorem])
# blog_body = "[eval sar.show(settings.SITE.models.blogs.LatestEntries)]"
# blog_body = "[show blogs.LatestEntries]"
ONEPAR = "\n\n" + get_paragraph(sep="\n\n") + "\n\n"
PARA = "\n\n" + get_paragraph(count=4, sep="\n\n") + "\n\n"
photos_children = []


def add(*args):
    photos_children.append(args)


add(
    _("Default formatting"), """
![alt text](/f/6)
The markdown command `![alt text](/f/6)` inserts the image inline, without any text wrapping.

The following image has been inserted using a `![alt text](/f/6)` without any
surrounding text inside a paragraph.

![alt text](/f/6)

And if you want the paragraph to be centered:

<p align="center">![alt text](/f/6)</p>

""" + get_paragraph() + PARA, Page, [])

add(
    _("Thumbnail"), """
Here is a small image: <img src="/f/6" alt="alt text" width="33%">
""" + get_paragraph() + PARA, Page, [])

add(
    _("Thumbnail left"), """
Here is a left-aligned image: <img src="/f/6" alt="alt text" width="33%" align="left">
""" + get_paragraph() + PARA, Page, [])

add(
    _("Wide"), """
Here is an image in a standalone paragraph and using the whole available text width:

<img src="/f/6" alt="alt text" width="100%">

""" + PARA, Page, [])

add(
    _("Gallery"),
    """

Here is a gallery of images:

<img src="/f/5" alt="alt text" width="30%">
<img src="/f/6" alt="alt text" width="30%">
<img src="/f/7" alt="alt text" width="30%">
<img src="/f/8" alt="alt text" width="30%">
<img src="/f/9" alt="alt text" width="30%">
<img src="/f/10" alt="alt text" width="30%">
<img src="/f/11" alt="alt text" width="30%">
<img src="/f/13" alt="alt text" width="30%">
<img src="/f/14" alt="alt text" width="30%">
"""
    + PARA, Page, [])

services_body = """
This page has a number of sections and subsections and a table of contents.

[TOC]

""" + ONEPAR

home_children = [(_("Services"), services_body, Page, [
    (_("Washing"), None, Node, []),
    (_("Drying"), "Drying is even more important than washing.", Node, [
        (_("Air drying"), None, Node, []),
        (_("Machine drying"), None, Node, [
            (_("Drying foos"), None, Node, []),
            (_("Drying bars"), None, Node, []),
            (_("Drying bazes"), None, Node, [])])]),
    (_("Ironing"), None, Node, []),
]), (_("Prices"), None, Page, []), (_("Photos"), None, Page, photos_children),
                 (_("About us"), None, Page, [
                     (_("Team"), None, Page, []),
                     (_("History"), None, Page, []),
                     (_("Contact"), None, Page, []),
                     (_("Terms & conditions"), None, Page, []),
                 ])]

# if dd.is_installed("blogs"):
#     home_children.append((_("Blog"), blog_body, "blogs.LatestEntries", []))
# if dd.is_installed("comments"):
#     home_children.append((_("Recent comments"), "", "comments.RecentComments", []))

site_pages = [(_("Home"), welcome, Page, home_children)]

# from pprint import pprint
# pprint(pages)


def objects():
    # Translation = rt.models.pages.Translation
    # for lc in settings.SITE.LANGUAGE_CHOICES:
    #     language = lc[0]
    #     kwargs = dict(language=language, ref='index')
    #     with translation.override(language):

    parent_nodes = []
    for lng in settings.SITE.languages:
        counter = {None: 0}
        # count = 0
        with translation.override(lng.django_code):

            def make_pages(pages, parent=None):
                # trans_parent = None
                for title, body, model, children in pages:
                    if model != Page:
                        raise Exception("{} {}".format(title, model))
                    kwargs = dict(title=title)
                    # kwargs = dd.str2kw("title", title, **kwargs)
                    # if filler:
                    #     kwargs.update(filler=filler)
                    # kwargs.update(page_type=rt.models.publisher.PageTypes.pages)
                    if body is None:
                        body = get_paragraph()

                    page_children = []

                    def collect_children(children, level=2):
                        body = ""
                        for child in children:
                            ctitle, cbody, cmodel, cchildren = child
                            if cmodel == Page:
                                page_children.append(child)
                            else:
                                assert cmodel == Node
                                body += ("#" * level) + " " + ctitle
                                body += PARA + (cbody or get_paragraph()) + PARA
                                if len(cchildren):
                                    body += PARA + collect_children(cchildren, level+1)
                        return body

                    body += PARA + collect_children(children)
                    if parent is None:
                        obj = rt.models.publisher.Page.objects.get(
                            special_page=SpecialPages.home,
                            language=lng.django_code)
                        obj.body = body
                    else:
                        kwargs.update(body=body)
                        if lng.suffix:
                            kwargs.update(
                                translated_from=parent_nodes[counter[None]])
                        kwargs.update(language=lng.django_code)
                        if dd.is_installed("publisher"):
                            kwargs.update(publishing_state='published')
                        obj = rt.models.publisher.Page(parent=parent, **kwargs)
                    yield obj
                    if not lng.suffix:
                        parent_nodes.append(obj)
                    # if lng.suffix:
                    #     kwargs.update(translated_from=parent_nodes[counter[None]])
                    #     yield Translation(parent=parent_nodes[counter[None]],
                    #         child=obj, language=lng.django_code)
                    #     # assert trans_parent is not None
                    #     # yield Translation(parent=trans_parent,
                    #     #     child=obj, language=lng.django_code)
                    # else:
                    #     parent_nodes.append(obj)
                    # trans_parent = obj
                    # ref = None
                    counter[None] += 1
                    # print("20230324", title, kwargs)
                    # count += 1
                    yield make_pages(page_children, obj)

            yield make_pages(site_pages)
