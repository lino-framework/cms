from lino.api import dd
if dd.get_plugin_setting("memo", "use_markup"):
    from lino_cms.lib.cms.fixtures.laundry_markdown import objects
else:
    from lino_cms.lib.cms.fixtures.laundry_nodes import objects
